//
//  Channels.swift
//  CivilChat
//
//  Created by vishal on 01/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import Foundation
import UIKit
internal class Channels {
    internal let id: String
    internal let name: String
    
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}
