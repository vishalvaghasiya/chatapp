//
//  AppDelegate.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import Foundation
import IQKeyboardManagerSwift
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
import Fabric
import Crashlytics
import Parse
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        FirebaseApp.configure()
        Fabric.with([Crashlytics.self])
        
        if UserDefaults.standard.value(forKey: "UserID") != nil && UserDefaults.standard.value(forKey: "UserID") as! String != ""{
            let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
            let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "MyConverstionViewController")
            navigationController.viewControllers = [rootViewController]
            self.window?.rootViewController = navigationController
        }
        else{
            
        }
        
        var _:Timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(timerCall), userInfo: nil, repeats: true)
        
//        application.registerForRemoteNotifications()
//        application.registerUserNotificationSettings(UIUserNotificationSettings.init())
        
        //MARK:- Parse
        Parse.initialize(with: ParseClientConfiguration(block: { (configuration) in
            configuration.applicationId = PARSE_APP_ID
            configuration.clientKey = PARSE_CLIENT_KEY
            configuration.server = PARSE_SERVER
            configuration.isLocalDatastoreEnabled = PARSE_LOCALDATASTORE_ENABLED
        }))
        
//        self.callAPI()
        return true
    }
    
    @objc func timerCall(){
        DBManager.shared.getMessageList()
    }
    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        if token == ""{
//        }
//        else{
//            AppData.sharedInstance.deviceToken = token
//        }
//    }
    
//    func callAPI(){
//        let query:PFQuery = PFQuery(className: "Apps")
//        query.findObjectsInBackground { (objects, error) in
//            if error == nil {
//                if objects!.count > 0 {
//                    for i in objects! {
//                        let temp = i
//                        let appName:String = temp.value(forKey: "AppName") as! String
//                        if appName == "CivilChat" {
//                            if temp.value(forKey: "IsOn") as! String == "false" {
//                                Crashlytics.sharedInstance().crash()
//                            }
//                            else{
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        if DBManager.shared.createDatabase() {
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

