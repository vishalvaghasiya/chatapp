//
//  Utility.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
import UIKit
import Reachability
import SVProgressHUD
import Alamofire
import Parse
@objc protocol  UtilityDelegate {
    @objc optional func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType)
}

class Utility {
    var window: UIWindow?
    var delegate: UtilityDelegate?
    
    class func createUser(email: String, password: String, name: String, partneremail:String , completionBlock: ((_ success: Bool, _ error: Error?) -> ())?) {
        let user = User()
        user.name = name
        user.username = email
        user.email = email
        user.password = password
        user.partneremail = partneremail

        user.signUpInBackground { (success, error) in
            completionBlock?(success, error)
        }
    }
    
    class func showAlert(_ title: String?, message: String?, viewController: UIViewController) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
        }))
        viewController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - Add shadow to View..
    class func addShadow(view: UIView) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 8.0
        //        caDemoButtonLayer?.shadowOpacity = 0.3
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func buttonAddShadow(view: UIButton) {
        let caDemoButtonLayer: CALayer? = view.layer
        caDemoButtonLayer?.frame = view.frame
        caDemoButtonLayer?.shadowRadius = 1.0
        caDemoButtonLayer?.cornerRadius = 8.0
        //        caDemoButtonLayer?.shadowOpacity = 0.3
        caDemoButtonLayer?.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(1.0))
    }
    
    class func showProgress(_ message: String) {
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        if(message == "") {
            SVProgressHUD.show()
        }
        else {
            SVProgressHUD.show(withStatus: message)
        }
    }

    class func dismissProgress() {
        SVProgressHUD.dismiss()
    }

    class func checkInternet() -> Bool {
        let networkReachability: Reachability = Reachability.forInternetConnection();
        let networkStatus : NetworkStatus = networkReachability.currentReachabilityStatus();
        if (networkStatus.rawValue == 0) {
            return false;
        } else {
            return true;
        }
    }
    
    func loginUser(userData: User, apiType: APIType) {
        if Utility.checkInternet(){
            PFUser.logInWithUsername(inBackground: userData.email!, password: userData.password!, block: { (user, error) in
                if error == nil {
                    print(user?.email, user)
                    self.delegate?.apiCallCompleted?(true, data: [:], error:"", apiType: apiType)
                }
                else{
                    self.delegate?.apiCallCompleted?(false, data: [:], error:(error?.localizedDescription)!, apiType: apiType)
                }
            })
        }
        else{
            self.delegate?.apiCallCompleted?(false, data: [:], error:"Please check internet connection and retry.", apiType: apiType)
        }
    }
    
    //MARK:- API Calling Function
    func UserLoginDetails(request: URLRequest, apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseData { (response) in
                if response.error == nil {
                    do {
                        let data:NSDictionary = try JSONSerialization.jsonObject(with: response.data!, options:.mutableContainers) as! NSDictionary
                        let message:String = data.value(forKey: "errorMsg") as! String
                        if data.value(forKey: "status")as! Int == 1 {
                            let temp:NSDictionary = data.value(forKey: "data") as! NSDictionary
                            UserDefaults.standard.set(temp.value(forKey: "id") as! String, forKey: "UserID")
                            UserDefaults.standard.set(temp.value(forKey: DB_FIRSTNAME) as! String, forKey: "UserName")
                            UserDefaults.standard.set(temp.value(forKey: "active") as! String, forKey: "isUserActive")
                            UserDefaults.standard.set(temp.value(forKey: "partner_name") as! String, forKey: "partnerName")
                            UserDefaults.standard.set(temp.value(forKey: "partner_id") as! String, forKey: "partnerID")
                            self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                        }
                        else {
                            self.delegate?.apiCallCompleted!(false, data: nil, error: message , apiType: apiType)
                        }
                    }
                    catch {
                        self.delegate?.apiCallCompleted!(false, data: nil, error: error.localizedDescription , apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: response.result.error?.localizedDescription, apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "Please check internet connection and retry.", apiType: apiType)
        }
    }
    
    func UserRegisterDetails(request: URLRequest, apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseData { (response) in
                if response.error == nil {
                    do {
                        let data:NSDictionary = try JSONSerialization.jsonObject(with: response.data!, options:.mutableContainers) as! NSDictionary
                        let message:String = data.value(forKey: "errorMsg") as! String
                        if data.value(forKey: "status")as! Int == 1 {
                            let temp:NSDictionary = data.value(forKey: "data") as! NSDictionary
//                            AppData.sharedInstance.isUserActive = temp.value(forKey: "active") as! String
                            UserDefaults.standard.set(temp.value(forKey: "active") as! String, forKey: "isUserActive")
                            UserDefaults.standard.set(temp.value(forKey: "id") as! String, forKey: "UserID")
                            UserDefaults.standard.set(temp.value(forKey: "partner_name") as! String, forKey: "partnerName")
                            UserDefaults.standard.set(temp.value(forKey: "partner_id") as! String, forKey: "partnerID")
                            self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                        }
                        else {
                            self.delegate?.apiCallCompleted!(false, data: nil, error: message , apiType: apiType)
                        }
                    }
                    catch {
                        self.delegate?.apiCallCompleted!(false, data: nil, error: error.localizedDescription , apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: response.result.error?.localizedDescription, apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "Please check internet connection and retry.", apiType: apiType)
        }
    }
    
    func UserForgotPassword(request: URLRequest, apiType:APIType){
        if Utility.checkInternet(){
            Alamofire.request(request).responseData { (response) in
                if response.error == nil {
                    do {
                        let data:NSDictionary = try JSONSerialization.jsonObject(with: response.data!, options:.mutableContainers) as! NSDictionary
                        let message:String = data.value(forKey: "errorMsg") as! String
                        if data.value(forKey: "status")as! Int == 1 {
                            self.delegate?.apiCallCompleted!(true, data: nil, error: message, apiType: apiType)
                        }
                        else {
                            self.delegate?.apiCallCompleted!(false, data: nil, error: message , apiType: apiType)
                        }
                    }
                    catch {
                        self.delegate?.apiCallCompleted!(false, data: nil, error: error.localizedDescription , apiType: apiType)
                    }
                }
                else{
                    self.delegate?.apiCallCompleted!(false, data: nil, error: response.result.error?.localizedDescription, apiType: apiType)
                }
            }
        }
        else{
            self.delegate?.apiCallCompleted!(false, data: nil, error: "Please check internet connection and retry.", apiType: apiType)
        }
    }
}
