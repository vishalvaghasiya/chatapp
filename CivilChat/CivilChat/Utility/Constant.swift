//
//  Constant.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
import UIKit

//"http://teslim.onusinfotech.com/"
//let BASE_URL                        = "http://labsol.biz.md-in-31.webhostbox.net/civilchat/"

//let URL_LOGIN                       = "\(BASE_URL)login.json"
//let URL_REGISTRATION                = "\(BASE_URL)register.json"
//let URL_FORGOTPASSWORD              = "\(BASE_URL)forgotPassword.json"

let GREYCOLOR                       = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0)
let DARKBLUE                        = UIColor(red: 11/255, green: 44/255, blue: 64/255, alpha: 1.0)

let SELECT_COLOR                    = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 0.5)
let WHITE_COLOR                     = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
let CLEAR_COLOR                     = UIColor.clear

let PARSE_APP_ID                                                =   "FiYzRrIYa1sU4MrGZMUNFkegpNisEFXfviLYAdlZ"
let PARSE_CLIENT_KEY                                            =   "K0I25aZuSiQ3GBBju5FTlsCRusKRcqqanrkvIXqq"
let PARSE_SERVER                                                =   "https://parseapi.back4app.com/"
let PARSE_LOCALDATASTORE_ENABLED                                =   true
let PARSE_FACEBOOK_APP_ID                                       =   "1643976149237893"
let DEFAULT_PARSE_USER_PASSWORD                                 =   "ASD@$#123"
let PARSE_QUERY_LIMIT                                           =   1000

enum UIUserInterfaceIdiom : Int
{
    case unspecified
    case phone
    case pad
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1366.0
}


