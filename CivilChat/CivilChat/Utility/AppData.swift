//
//  AppData.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import Foundation
import UIKit

class AppData {
    
    var storyboard : UIStoryboard!;
    var appDelegate: AppDelegate!;
    
    var user: User?
    
    var email:String = String()
    var partnerID: String = String()
    var userID:String = String()
//    var partnerID:String = String()
    
    static let sharedInstance: AppData = {
        let instance = AppData()
        // some additional setup
        return instance
    }()

}
