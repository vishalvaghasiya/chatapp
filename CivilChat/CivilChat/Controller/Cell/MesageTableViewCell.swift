//
//  MesageTableViewCell.swift
//  CivilChat
//
//  Created by admin on 02/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import UIKit

class MesageTableViewCell: UITableViewCell {

    @IBOutlet var leftmessageViewBubble: UIView!
    @IBOutlet var leftView: UIView!
    @IBOutlet var leftTimeLabel: UILabel!
    @IBOutlet var leftMessageText: UITextView!
    
    @IBOutlet var leftImageView: UIView!
    @IBOutlet var leftImage: UIImageView!
    @IBOutlet var leftImageTimeLabel: UILabel!
    
    @IBOutlet var rightMessageBubble: UIView!
    @IBOutlet var rightView: UIView!
    @IBOutlet var readImage: UIImageView!
    @IBOutlet var righttimeLabel: UILabel!
    @IBOutlet var rightMessageText: UITextView!
    @IBOutlet var rightDeleteButtonClick: UIButton!
    
    @IBOutlet var rightImageView: UIView!
    @IBOutlet var rightImage: UIImageView!
    @IBOutlet var rightImageReadImageview: UIImageView!
    @IBOutlet var rightImageTimeLabel: UILabel!
    @IBOutlet var rightImageDeleteButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }

}
