//
//  StartConverstionViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase
class StartConverstionViewController: UIViewController {

    @IBOutlet var topicTextField: UITextField!
    @IBOutlet var spouseNameLabel: UILabel!
    @IBOutlet var spouseSubTitle: UILabel!
    
    var channelRef: DatabaseReference = Database.database().reference()
    private lazy var messageRef: DatabaseReference = self.channelRef.child("conversation")
    
    var ref: DatabaseReference = Database.database().reference()
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    
    var memberList:[String] = []
    var selected:Bool = Bool()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let isUserActive:String = UserDefaults.standard.value(forKey: "isUserActive") as! String
//        let partnerName:String = UserDefaults.standard.value(forKey: "partnerName") as! String
//        if isUserActive == "1"{
//            self.spouseNameLabel.text = partnerName
//        }
//        else{
//            self.spouseNameLabel.text = ""
//        }
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createTopicButtonClick(_ sender: UIButton) {
//        let isUserActive:String = UserDefaults.standard.value(forKey: "isUserActive") as! String
//        if isUserActive == "1"{
            if self.topicTextField.text != "" {
                let ID:Int = Int((Date().timeIntervalSince1970 * 1000))
                let userID = UserDefaults.standard.value(forKey: "UserID")
                let userName = UserDefaults.standard.value(forKey: "UserName")
                var partnerID:String = ""
                if UserDefaults.standard.value(forKey: "partnerID") != nil {
                    partnerID = UserDefaults.standard.value(forKey: "partnerID") as! String
                }
                memberList.append(UserDefaults.standard.value(forKey: "UserID") as! String)
                memberList.append(partnerID)
                let temp = Date().timeIntervalSince1970 * 1000
                let CurruntTimeStamp = Int(temp)
                // 1
                let itemRef = messageRef.childByAutoId()
                // 2
                let messageItem = [
                    "id": "\(ID)",
                    "userId": "\(userID ?? "")",
                    "userName": "\(userName ?? "")",
                    "conversationName": self.topicTextField.text!,
                    "timestamp":"\(CurruntTimeStamp)",
                    "member": memberList
                    ] as [String : Any]
                // 3
                itemRef.setValue(messageItem)
                let alertController = UIAlertController(title: "Message", message: "Conversation add successfully.", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { (action: UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            else{
                Utility.showAlert("", message: "Please enter the topic name.", viewController: self)
            }
//        }
//        else{
//            Utility.showAlert("", message: "Sorry your account is not active, please contact to Administrator.", viewController: self)
//        }
    }
}
