//
//  ChatViewController1.swift
//  CivilChat
//
//  Created by admin on 02/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth
import FirebaseCore
import SDWebImage
import Alamofire
import ToneAnalyzer
class ChatViewController1: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var messageTextView: UIView!
    @IBOutlet var chatTableview: UITableView!
    @IBOutlet var messageTextFields: UITextField!
    
    private let imageURLNotSetKey = ""
    private var locationSetKey = "LOCATIONNOTSET"
    
    var userID : String = AppData.sharedInstance.userID
    var senderId:String = AppData.sharedInstance.userID
    var conversationName:String = ""
    var channelRef: DatabaseReference = Database.database().reference()
    //    var userObj : User = User()
    
    var storage = Storage.storage()
    private lazy var messageRef: DatabaseReference = self.channelRef.child("messages")
    //    private lazy var conversationRef: DatabaseReference = self.channelRef
    private lazy var conversationRef = Database.database().reference().child("conversation")
    fileprivate lazy var storageRef: StorageReference = Storage.storage().reference(forURL: "gs://civilchat-3ed4c.appspot.com/")
    
    private lazy var userIsTypingRef: DatabaseReference = self.channelRef.child("typingIndicator").child(AppData.sharedInstance.userID)
    private lazy var usersTypingQuery: DatabaseQuery = self.channelRef.child("typingIndicator").queryOrderedByValue().queryEqual(toValue: true)
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    
    @IBOutlet var conversationTitle: UILabel!
    
    var curruntUserID:String = String()
    var senderDisplayName:String = String()
    
    var selectedIndex : Int = 0
    let defaults = UserDefaults.standard
    var resolveView = UIView()
    var flag : Bool?
    var channelID:String = String()
    
    var messageDetails:Messages = Messages()
    
    var channel: Channels? {
        didSet {
            title = channel?.name
        }
    }
    
    var addMessage:NSMutableArray = NSMutableArray()
    var addMessageKey:NSMutableArray = NSMutableArray()
    
    var messageIndex:Int = -1
    var bounds = UIScreen.main.bounds
    var screenWidth:CGFloat = 0.0
    
    var listProfanWords:[String] = []
    var textMessage:String = ""
    var isProfaneWords:Bool = Bool()
    var profane:String = ""
    var conversationKey:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(refreshData), name: Notification.Name("refreshData"), object: nil)
        
        self.readJsonFile()
        
        self.messageTextView.layer.cornerRadius = self.messageTextView.frame.height / 2;
        self.messageTextView.clipsToBounds = true
        self.messageTextView.layer.borderWidth = 1.0
        self.messageTextView.layer.borderColor = UIColor(red: 241/255, green: 241/255, blue: 241/255, alpha: 1.0).cgColor
        
        screenWidth = bounds.size.width
        
        self.navigationController?.isNavigationBarHidden = true
        self.channelRef = Database.database().reference().child("channels/\(channelID)/")
        self.senderId = UserDefaults.standard.value(forKey: "UserID") as! String
        if UserDefaults.standard.value(forKey: "UserName") != nil {
            self.senderDisplayName = UserDefaults.standard.value(forKey: "UserName") as! String
        }
        else{
            self.senderDisplayName = ""
        }
        if UserDefaults.standard.value(forKey: "UserID") != nil {
            curruntUserID = UserDefaults.standard.value(forKey: "UserID") as! String
        }
        else{
            curruntUserID = ""
        }
        
        observeMessages()
        self.chatTableview.delegate = self
        self.chatTableview.dataSource = self
        self.chatTableview.reloadData()
        readByMessage()
        
        self.title = conversationName
        self.conversationTitle.text = conversationName
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func readJsonFile(){
        let file = Bundle.main.path(forResource: "Profanity", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        do {
            listProfanWords = try JSONSerialization.jsonObject(with: data!, options: []) as! [String]
        } catch{
            print("Error",error.localizedDescription)
        }
    }
    
    @objc func refreshData(){
        //        self.chatTableview.delegate = self
        //        self.chatTableview.dataSource = self
        ////        observeMessages()
        //        self.chatTableview.reloadData()
        //        self.chatTableview.setNeedsLayout()
        //        self.listing_scrollToBottom()
        //        self.chatTableview.setNeedsLayout()
        //        self.chatTableview.reloadData()
    }
    
    //MARK: Profane textfield Delegate Method
    @IBAction func profanFilterTextField(_ sender: UITextField) {
        if containsSwearWord(text: sender.text!, swearWords: listProfanWords) {
            self.textMessage = sender.text!
            isProfaneWords = true
            profane = "1"
        }
        else{
            self.textMessage = sender.text!
            isProfaneWords = false
            profane = "0"
        }
    }
    
    //MARK: Profan Filter Function
    func containsSwearWord(text: String, swearWords: [String]) -> Bool {
        let splitNumber : [String] = text.components(separatedBy: " ")
        var isval:Bool = false
        for temp in splitNumber {
            for temp2 in swearWords {
                if temp2 == temp {
                    isval = true
                    break
                }
            }
            //            if swearWords.contains(temp) {
            //                isval = true
            //                break
            //            }
            //            return swearWords.reduce(false) {
            //                $0 || temp.contains($1.lowercased())
            //                isval = false
            //            }
        }
        return isval
    }
    
    //MARK: Button CLick Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func attachButtonClick(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Capture Image", style: .default, handler: { (action) in
            self.cameraImageActionSheet()
        }))
        alert.addAction(UIAlertAction(title: "Pick From PhotoGalary", style: .default, handler: { (action) in
            self.photoLibraryImageActionSheet()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Image ActionSheet
    func cameraImageActionSheet(){
        let picker = UIImagePickerController()
        picker.delegate = self
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            picker.sourceType = UIImagePickerControllerSourceType.camera
        }
        else {
            Utility.showAlert(self.title, message: "Camera not available on your device", viewController: self)
        }
        present(picker, animated: true, completion:nil)
    }
    func photoLibraryImageActionSheet(){
        let picker = UIImagePickerController()
        picker.delegate = self
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        present(picker, animated: true, completion:nil)
    }
    
    @IBAction func sendMessageButtonClick(_ sender: UIButton) {
        if self.messageTextFields.text != "" {
            //            self.sendTextMessage()
            GetToneFunction()
        }
    }
    
    func sendTextMessage(){
        DispatchQueue.main.async {
            self.messageTextFields.text = self.textMessage
            let temp = Date().timeIntervalSince1970 * 1000
            let CurruntTimeStamp = Int(temp)
            // 1
            let itemRef = self.messageRef.childByAutoId()
            // 2
            let messageItem = [
                "senderId": self.senderId,
                "senderName": self.senderDisplayName,
                "text": self.messageTextFields.text!,
                "read": "0",
                "ismediamessage": "0",
                "receivervisible": "0",
                "sendervisible": "1",
                "isprofaneWord":self.profane,
                "timestamp":"\(CurruntTimeStamp)"
                ] as [String : Any]
            // 3
            itemRef.setValue(messageItem)
            
            self.upsateTimestamp()
            
            self.messageDetails.messageid = itemRef.key
            self.messageDetails.ismediamessage = "0"
            self.messageDetails.read = "0"
            self.messageDetails.senderId = self.senderId
            self.messageDetails.senderName = self.senderDisplayName
            self.messageDetails.text = self.messageTextFields.text!
            self.messageDetails.timestamp = "\(CurruntTimeStamp)"
            self.messageDetails.receivervisible = "0"
            self.messageDetails.sendervisible = "1"
            self.messageDetails.channelid = self.channelID
            
            if self.isProfaneWords == true {
            }
            else{
                DBManager.shared.insertMessageData(messageData: self.messageDetails)
                self.isProfaneWords = false
            }
            
            self.messageTextFields.text = ""
            self.messageTextFields.resignFirstResponder()
            
            self.messageRef = self.channelRef.child("messages")
            let messageQuery = self.messageRef.queryLimited(toLast:50)
            // messages being written to the Firebase DB
            DispatchQueue.main.async {
                self.newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
                    let messageData:NSDictionary = snapshot.value as! NSDictionary
                    self.chatTableview.reloadData()
                    self.chatTableview.setNeedsLayout()
                    self.listing_scrollToBottom()
                    self.chatTableview.setNeedsLayout()
                    self.chatTableview.reloadData()
                })
            }
        }
        
    }
    
    func GetToneFunction(){
        let toneAnalyzer = ToneAnalyzer(version: "2017-09-21", apiKey: "BsuO0cDRldoMhVrFxRtkNCGSNdL8yHeX3OecghSwtq-_")
        toneAnalyzer.serviceURL = "https://gateway-lon.watsonplatform.net/tone-analyzer/api"
        if messageTextFields.text != "" {
            let customerFeedback = messageTextFields.text!
            toneAnalyzer.tone(toneContent: .text(customerFeedback)) { (response) in
                print(response)
                let tones = response.documentTone.tones
                if tones!.count > 0 {
                    for temp in tones! {
                        let popMessage = "Pause. Chat App detects that your conversation may be getting emotional. Take a second and allow reasoning and common sense to control the conversation."
                        let popMessage2 = "Chat App detected that you are upset, therefore, didn’t send your message.  Please take a moment and compose yourself before continuing your conversation."
                        
                        if temp.toneID == "anger" || temp.toneID == "sadness" || temp.toneID == "fear" {
                            if (temp.score > 0.75) {
                                DispatchQueue.main.async {
                                    Utility.showAlert("", message: popMessage, viewController: self)
                                }
                            }
                        }
                        else{
                            if (temp.score < 0.5){
                            }
                            else if (temp.score > 0.5 && temp.score < 0.75) {
                                if temp.toneID != "analytical" || temp.toneID != "confident" || temp.toneID != "tentative" {
                                    DispatchQueue.main.async {
                                        Utility.showAlert("", message: popMessage2, viewController: self)
                                    }
                                }
                                self.sendTextMessage()
                            }
                            else if (temp.score > 0.75) {
                                self.sendTextMessage()
                            }
                        }
                    }
                } else {
                    self.sendTextMessage()
                }
            }
        }
    }
    
    func sendPhotoMessage() -> String? {
        let itemRef = messageRef.childByAutoId()
        let temp = Date().timeIntervalSince1970 * 1000
        let CurruntTimeStamp = Int(temp)
        let messageItem = [
            "photoURL":imageURLNotSetKey,
            "senderId":senderId,
            "senderName": senderDisplayName,
            "read": "0",
            "ismediamessage": "1",
            "receivervisible": "0",
            "sendervisible": "1",
            "uploading": "false",
            "isprofaneWord":"0",
            "timestamp":"\(CurruntTimeStamp)"
            ] as [String : Any]
        
        itemRef.setValue(messageItem)
        self.upsateTimestamp()
        
        messageDetails.messageid = itemRef.key
        messageDetails.ismediamessage = "1"
        messageDetails.read = "0"
        messageDetails.senderId = senderId
        messageDetails.senderName = senderDisplayName
        messageDetails.text = ""
        messageDetails.timestamp = "\(CurruntTimeStamp)"
        messageDetails.receivervisible = "0"
        messageDetails.sendervisible = "1"
        messageDetails.channelid = channelID
        
        DBManager.shared.insertMessageData(messageData: messageDetails)
        
        return itemRef.key
    }
    
    func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = messageRef.child(key)
        itemRef.updateChildValues(["photoURL":url])
        self.observeMessages()
        self.chatTableview.reloadData()
    }
    
    func upsateTimestamp(){
        // Update Conversation timestamp Field.
        let conversationQuery = conversationRef.queryOrderedByValue()
        // messages being written to the Firebase DB
        conversationQuery.observe(.childAdded, with: { (snapshot) in
            if((snapshot.value as AnyObject).count != nil)
            {
                let temp:NSDictionary = snapshot.value as! NSDictionary
                if temp.value(forKey: "id") as! String == self.channelID{
                    // Update firebase Field
                    let temp = Date().timeIntervalSince1970 * 1000
                    let CurruntTimeStamp = Int(temp)
                    self.conversationRef.child(snapshot.key).updateChildValues(["timestamp" : "\(CurruntTimeStamp)"])
                }
            }
        })
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    private func observeMessages() {
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryLimited(toLast:50)
        self.addMessage.removeAllObjects()
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            let allKeys:[String] = messageData.allKeys as! [String]
            if allKeys.contains("senderId") && allKeys.contains("sendervisible") && allKeys.contains("receivervisible") {
                if messageData.count > 0 {
                    if  messageData.value(forKey: "senderId") as! String != self.curruntUserID {
                        if messageData.value(forKey: "receivervisible") as! String == "1" {
                            self.addMessage.add(messageData)
                            self.addMessageKey.add(snapshot.key)
                        }
                    }
                    else{
                        if messageData.value(forKey: "sendervisible") as! String == "1" {
                            self.addMessage.add(messageData)
                            self.addMessageKey.add(snapshot.key)
                        }
                    }
                    self.chatTableview.reloadData()
                    self.listing_scrollToBottom()
                }
            }
        })
        
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            if messageData.count > 0 {
                let allKeys:[String] = messageData.allKeys as! [String]
                if allKeys.contains("senderId") && allKeys.contains("sendervisible") && allKeys.contains("receivervisible") {
                    if  messageData.value(forKey: "senderId") as! String != self.curruntUserID {
                        if messageData.value(forKey: "receivervisible") as! String == "1" {
                            var count = 0
                            var isInclude:Bool = false
                            for i in self.addMessage {
                                count = count + 1
                                let temp:NSDictionary  = i as! NSDictionary
                                if temp.value(forKey: "timestamp") as! String == messageData.value(forKey: "timestamp") as! String {
                                    self.addMessage.replaceObject(at: (count - 1), with: messageData)
                                    self.addMessageKey.replaceObject(at: (count - 1), with: snapshot.key)
                                    isInclude = true
                                }
                                else{
                                    isInclude = false
                                }
                            }
                            if isInclude == false {
                                self.addMessage.add(messageData)
                                self.addMessageKey.add(snapshot.key)
                            }
                        }
                    }
                    else {
                        if messageData.value(forKey: "sendervisible") as! String == "1" {
                            if messageData.value(forKey: "senderId") as! String == self.curruntUserID {
                                // VK
                                var count = 0
                                for i in self.addMessage {
                                    count = count + 1
                                    let temp:NSDictionary  = i as! NSDictionary
                                    if temp.value(forKey: "timestamp") as! String == messageData.value(forKey: "timestamp") as! String {
                                        self.addMessage.replaceObject(at: (count - 1), with: messageData)
                                        self.addMessageKey.replaceObject(at: (count - 1), with: snapshot.key)
                                    }
                                    else{
                                    }
                                }
                                // VK
                            }
                            else{
                                self.addMessage.add(messageData)
                                self.addMessageKey.add(snapshot.key)
                            }
                        }
                        else {
                            if messageData.value(forKey: "sendervisible") as! String == "0" {
                                self.addMessage.remove(messageData)
                                self.addMessageKey.remove(snapshot.key)
                                self.chatTableview.reloadData()
                                
                                var count = 0
                                for i in self.addMessage {
                                    count = count + 1
                                    let temp:NSDictionary  = i as! NSDictionary
                                    if temp.value(forKey: "timestamp") as! String == messageData.value(forKey: "timestamp") as! String {
                                        self.addMessage.removeObject(at: count - 1)
                                        //                                    self.addMessageKey.removeObject(at: count - 1)
                                        self.chatTableview.reloadData()
                                    }
                                }
                            }
                        }
                    }
                }
                self.chatTableview.reloadData()
                self.listing_scrollToBottom()
            }
        })
        
        updatedMessageRefHandle = messageRef.observe(.childRemoved, with: { (snapshot) in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            if messageData.count > 0 {
                let allKeys:[String] = messageData.allKeys as! [String]
                if allKeys.contains("senderId") && allKeys.contains("sendervisible") && allKeys.contains("receivervisible") {
                    if  messageData.value(forKey: "senderId") as! String == self.curruntUserID {
                        if messageData.value(forKey: "receivervisible") as! String == "0" {
                            self.addMessage.remove(messageData)
                            self.addMessageKey.remove(snapshot.key)
                            self.chatTableview.reloadData()
                        }
                    }
                }
                self.chatTableview.reloadData()
                self.listing_scrollToBottom()
            }
        })
    }
    
    func listing_scrollToBottom() {
        let delay = 0.1 * Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            if self.addMessage.count > 0 {
                let lastRowIndexPath = NSIndexPath(row: self.addMessage.count-1, section: 0)
                self.chatTableview.scrollToRow(at: lastRowIndexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: false)
            }
        }
    }
    
    //MARK: message Read Function
    func readByMessage(){
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryOrderedByValue()
        // messages being written to the Firebase DB
        messageQuery.observe(.childAdded, with: { (snapshot) in
            let messageData = snapshot.value as! Dictionary<String, String>
            if((snapshot.value as AnyObject).count != nil)
            {
                if messageData["senderId"] != self.curruntUserID {
                    if messageData["receivervisible"] == "1"{
                        self.messageRef.child(snapshot.key).updateChildValues(["read" : "1"])
                    }
                }
            }
        })
    }
    
    // MARK: Tableview Delegate Method
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addMessage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MesageTableViewCell") as! MesageTableViewCell
        let leftCell = ((screenWidth - 30) / 2 );
        let rightCell = ((screenWidth - 30 ) / 2);
        let maxWidth = leftCell
        let data:NSDictionary = self.addMessage[indexPath.row] as! NSDictionary
        
        if data.value(forKey: "senderId") as! String != curruntUserID {
            if data.value(forKey: "ismediamessage") as! String == "0" {
                
                cell.leftMessageText.text = data.value(forKey: "text") as! String
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.leftTimeLabel.text = strtime
                
                cell.rightImageView.isHidden = true
                cell.leftImageView.isHidden = true
                cell.leftmessageViewBubble.isHidden = false
                cell.leftView.isHidden = false
                cell.rightMessageBubble.isHidden = true
                cell.rightView.isHidden = true
                
                cell.leftView.frame = CGRect(x: 15, y: 5, width: maxWidth, height: cell.leftView.frame.height)
                
                cell.leftmessageViewBubble.frame = CGRect(x: 0, y: 0, width: maxWidth , height: cell.leftMessageText.contentSize.height + 20)
                cell.leftmessageViewBubble.backgroundColor = DARKBLUE
                
                cell.leftMessageText.frame = CGRect(x: 5, y: 5, width: maxWidth - 5 , height: cell.leftMessageText.contentSize.height)
                
                cell.leftTimeLabel.frame = CGRect(x: 0, y: cell.leftmessageViewBubble.frame.height, width: maxWidth, height: cell.leftTimeLabel.frame.height)
                
                cell.leftmessageViewBubble.layer.cornerRadius = 10.0//(cell.leftmessageViewBubble.frame.height / 2);
                cell.leftmessageViewBubble.clipsToBounds = true
                //                    cell.leftmessageViewBubble.autoresizingMask = cell.leftmessageViewBubble.autoresizingMask
                self.chatTableview.rowHeight = cell.leftMessageText.contentSize.height + 45
            }
            else{
                cell.rightImageView.isHidden = true
                cell.leftImageView.isHidden = false
                
                cell.rightView.isHidden = true
                cell.leftView.isHidden = true
                
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.leftImageTimeLabel.text = strtime
                
                self.chatTableview.rowHeight = 130.0
                let url:String = data.value(forKey: "photoURL") as! String
                if url != "" {
                    DispatchQueue.main.async {
                        let gsReference = self.storage.reference(forURL: url )
                        gsReference.downloadURL(completion: { (imageURL, error) in
                            if error == nil {
                                cell.leftImage.sd_setImage(with: imageURL, completed: { (image, error, type, url) in
                                    cell.rightImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions.cacheMemoryOnly, completed: { (image, error, type, url) in
                                        
                                    })
                                })
                            }
                        })
                    }
                }
                self.chatTableview.rowHeight = 130.0
            }
        }
        else {
            if data.value(forKey: "ismediamessage") as! String == "0" {
                cell.rightMessageText.text = data.value(forKey: "text") as! String
                
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.righttimeLabel.text = strtime
                
                cell.rightImageView.isHidden = true
                cell.leftImageView.isHidden = true
                cell.leftmessageViewBubble.isHidden = true
                cell.leftView.isHidden = true
                cell.rightMessageBubble.isHidden = false
                cell.rightView.isHidden = false
                cell.readImage.isHidden = false
                
                if data.value(forKey: "isprofaneWord") as! String == "1"{
                    cell.readImage.image = UIImage(named: "Icon_error")
                }
                else if data.value(forKey: "isprofaneWord") as! String == "0"{
                    if data.value(forKey: "read") as! String == "1"{
                        cell.readImage.image = UIImage(named: "read")
                    }
                    else{
                        cell.readImage.image = UIImage(named: "unread")
                    }
                }
                
                cell.rightView.frame = CGRect(x: rightCell + 15, y: 5, width: maxWidth , height: cell.rightView.frame.height)
                
                cell.rightMessageBubble.frame = CGRect(x: 0, y: 0, width: maxWidth , height: cell.rightMessageText.contentSize.height + 20)
                cell.rightMessageBubble.backgroundColor = GREYCOLOR
                
                cell.rightMessageText.frame = CGRect(x: 0, y: 5, width: maxWidth , height: cell.rightMessageText.contentSize.height + 5)
                
                cell.readImage.frame = CGRect(x: cell.rightMessageBubble.frame.width - cell.readImage.frame.width , y: cell.rightMessageBubble.frame.height , width: cell.readImage.frame.width , height: cell.readImage.frame.height)
                
                cell.righttimeLabel.frame = CGRect(x: 0 , y: cell.rightMessageBubble.frame.height , width: cell.rightMessageBubble.frame.width - cell.readImage.frame.width - 15 , height: cell.righttimeLabel.frame.height)
                
                cell.rightMessageBubble.layer.cornerRadius = 10.0//cell.rightMessageBubble.frame.height / 2;
                cell.rightMessageBubble.clipsToBounds = true
                
                cell.rightDeleteButtonClick.frame = CGRect(x: 0, y: 0, width: maxWidth + 20 , height: cell.rightMessageText.contentSize.height + 20)
                cell.rightDeleteButtonClick.tag = indexPath.row
                cell.rightDeleteButtonClick.addTarget(self, action: #selector(getTag(sender:)), for: .touchDown)
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteMessage(sender:)))
                cell.rightDeleteButtonClick.addGestureRecognizer(longGesture)
                
                self.chatTableview.rowHeight = cell.rightMessageText.contentSize.height + 45
            }
            else{
                cell.rightView.isHidden = true
                cell.leftView.isHidden = true
                cell.rightImageView.isHidden = false
                cell.leftImageView.isHidden = true
                
                let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                let formatter = DateFormatter()
                let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
                formatter.dateFormat = "hh:mm a"
                let strtime = formatter.string(from: unixTimestamp as Date)
                cell.rightImageTimeLabel.text = strtime
                
                let url:String = data.value(forKey: "photoURL") as! String
                if url != "" {
                    DispatchQueue.main.async {
                        let gsReference = self.storage.reference(forURL: url )
                        gsReference.downloadURL(completion: { (imageURL, error) in
                            if error == nil {
                                cell.rightImage.sd_setImage(with: imageURL, completed: { (image, error, type, url) in
                                    cell.rightImage.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "placeholder"), options: SDWebImageOptions.cacheMemoryOnly, completed: { (image, error, type, url) in
                                        
                                    })
                                })
                            }
                        })
                    }
                }
                cell.rightImageDeleteButton.tag = indexPath.row
                cell.rightImageDeleteButton.addTarget(self, action: #selector(getTag(sender:)), for: .touchDown)
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(deleteMessage(sender:)))
                cell.rightImageDeleteButton.addGestureRecognizer(longGesture)
                
                self.chatTableview.rowHeight = 130.0
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func getTag(sender:UIButton){
        self.messageIndex = sender.tag
    }
    
    @objc func deleteMessage(sender:UIGestureRecognizer){
        let data:NSDictionary = self.addMessage[self.messageIndex] as! NSDictionary
        let key = addMessageKey[self.messageIndex] as! String
        if data.value(forKey: "receivervisible") as! String == "0" {
            if data.value(forKey: "isprofaneWord") as! String == "0" {
                if data.value(forKey: "receivervisible") as! String == "0"{
                    let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
                    alert.addAction(UIAlertAction(title: "Delete for me", style: .default, handler: { (action) in
                        self.deleteForMe(messageKey: key)
                    }))
                    alert.addAction(UIAlertAction(title: "Delete for everyone", style: .default, handler: { (action) in
                        self.deleteEveryone(messageKey: key)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                        
                    }))
                    present(alert, animated: true, completion: nil)
                }
            }
            else{
                if data.value(forKey: "receivervisible") as! String == "0"{
                    let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
                    //                    let formatter = DateFormatter()
                    let unixTimestamp = Date(timeIntervalSince1970: epocTime)//NSDate(timeIntervalSince1970: epocTime)
                    let secondDate = unixTimestamp
                    let response = Date().offset(from: secondDate)//NSDate.friendlyIntervalBetweenDates(firstDate, secondDate: secondDate)
                    let distance:Int = Int(response)!
                    if distance < 2 {
                        let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (action) in
                            self.deleteEveryone(messageKey: key)
                        }))
                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                            
                        }))
                        present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func deleteForMe(messageKey:String){
        messageRef = channelRef.child("messages")
        let messageQuery = messageRef.queryOrderedByValue()
        // messages being written to the Firebase DB
        messageQuery.observe(.childAdded, with: { (snapshot) in
            let messageData = snapshot.value as! Dictionary<String, String>
            if((snapshot.value as AnyObject).count != nil)
            {
                if snapshot.key == messageKey {
                    if messageData["sendervisible"] == "1"{
                        self.messageRef.child(snapshot.key).updateChildValues(["sendervisible" : "0"])
                        //                        self.observeMessages()
                        self.chatTableview.reloadData()
                        self.listing_scrollToBottom()
                        self.chatTableview.setNeedsLayout()
                    }
                }
            }
        })
    }
    
    func deleteEveryone(messageKey:String){
        let ref = Database.database().reference().child("channels").child("\(channelID)").child("messages")
        ref.queryOrderedByKey().observe(.childAdded) { (snapshot) in
            if snapshot.key == messageKey {
                snapshot.ref.removeValue(completionBlock: { (error, reference) in
                    if error == nil {
                        DBManager.shared.deleteRecoding(messageID: messageKey)
                        self.chatTableview.reloadData()
                        self.listing_scrollToBottom()
                        self.chatTableview.setNeedsLayout()
                    }
                })
            }
        }
    }
}

// MARK: Image Picker Delegate
extension ChatViewController1: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        if let key = sendPhotoMessage() {
            let data = UIImageJPEGRepresentation(image, 0.1)!
            let fileManager = FileManager.default;
            let url = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent("file.png")
            do {
                if(fileManager.fileExists(atPath: url.path)) {
                    try fileManager.removeItem(at: url)
                }
                try data.write(to: url)
            }
            catch let error {
                print(error)
            }
            let path = "\(Int(Date.timeIntervalSinceReferenceDate * 1000))/\(Int(Date.timeIntervalSinceReferenceDate * 1000)).png"
            storageRef.child(path).putFile(from: url, metadata: nil, completion: { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error.localizedDescription)")
                    return
                }
                // 7
                let url:URL = (metadata?.downloadURL())!
                //                self.setImageURL(self.storageRef.child("\(url)").description, forPhotoMessageWithKey: key)
                self.setImageURL("\(url)", forPhotoMessageWithKey: key)
            })
        }
        picker.dismiss(animated: true, completion:nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion:nil)
    }
}
