//
//  MyConverstionViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
//import Alamofire
import FirebaseCore
import FirebaseDatabase
import FirebaseStorage
class MyConverstionViewController: UIViewController, UtilityDelegate, UITableViewDelegate , UITableViewDataSource{
    
    @IBOutlet var popupView: UIView!
    @IBOutlet var messageView: UIView!
    @IBOutlet var conversationTableview: UITableView!
    @IBOutlet var popUPMessageTitile: UILabel!
    
    var channelRef: DatabaseReference = Database.database().reference()
    private lazy var messageRef: DatabaseReference = self.channelRef.child("conversation")
    
    private var newMessageRefHandle: DatabaseHandle?
    private var updatedMessageRefHandle: DatabaseHandle?
    
    var conversations:NSMutableArray = NSMutableArray()
    var conversationsKey:NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var noConversationFound: UILabel!
    var curruntUserID:String = ""
    var utility: Utility = Utility()
    
    var refreshControl: UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        conversationTableview.delegate = self
        conversationTableview.dataSource = self
        
        self.messageView.layer.cornerRadius = 10.0
        self.messageView.clipsToBounds = true
        self.conversationTableview.isHidden = true
        Utility.showProgress("")
        self.curruntUserID = UserDefaults.standard.value(forKey: "UserID") as! String
        
        noConversationFound.isHidden = true
        
        // Alert Message POPUP
        self.popUPMessageTitile.text = "Message"
        if UserDefaults.standard.value(forKey: "isUserActive") != nil {
            let isUserActive:String = UserDefaults.standard.value(forKey: "isUserActive") as! String
            if isUserActive == "1"{
                self.popupView.isHidden = true
            }
            else {
                self.popupView.isHidden = true
            }
        } else {
            self.popupView.isHidden = true
        }
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        conversationTableview.addSubview(refreshControl)
        observeMessages()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let epocTime = TimeInterval("1515820749927.44")! / 1000
//        let unixTimestamp = Date(timeIntervalSince1970: epocTime)//NSDate(timeIntervalSince1970: epocTime)
//        let secondDate = unixTimestamp
//
//        let myResponse = Date().offsetDate(from: secondDate)
    }
    
    deinit {
        if let refHandle = newMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
        if let refHandle = updatedMessageRefHandle {
            messageRef.removeObserver(withHandle: refHandle)
        }
    }
    
    @objc func refresh(){
        refreshMessages()
        self.refreshControl.endRefreshing()
    }
    
    private func refreshMessages() {
        self.conversationTableview.reloadData()
//        messageRef = channelRef.child("conversation")
//        let messageQuery = messageRef.queryLimited(toLast:10000)
//        Utility.dismissProgress()
        // messages being written to the Firebase DB
//        self.conversations.removeAllObjects()
//        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
//            if snapshot.value != nil {
//                self.conversationTableview.isHidden = false
//                let myConversations:NSDictionary = snapshot.value as! NSDictionary
//                if myConversations.count > 0 {
//                    Utility.dismissProgress()
//                    let arr:[String] = myConversations.value(forKey: "member") as! [String]
//                    if arr.contains(self.curruntUserID) {
//                        self.conversations.add(myConversations)
//                    }
//                    self.conversationTableview.reloadData()
//                }
//                else{
//                    //                    Utility.dismissProgress()
//                    self.conversationTableview.isHidden = true
//                }
//            }
//            else{
//                //                Utility.dismissProgress()
//                self.conversationTableview.isHidden = true
//            }
//        })
    }
    
    private func observeMessages() {
        messageRef = channelRef.child("conversation")
        let messageQuery = messageRef.queryLimited(toLast:10000)
        Utility.showProgress("")
        //        self.refreshMessages()
        Utility.dismissProgress()
        // messages being written to the Firebase DB
        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            if snapshot.value != nil {
                self.conversationTableview.isHidden = false
                let myConversations:NSDictionary = snapshot.value as! NSDictionary
                if myConversations.count > 0 {
                    Utility.dismissProgress()
                    let arr:[String] = myConversations.value(forKey: "member") as! [String]
                    if arr.contains(self.curruntUserID) {
                        self.conversations.add(myConversations)
                        self.conversationsKey.add(snapshot.key)
                    }
                    self.conversationTableview.reloadData()
                }
                else{
                    self.conversationTableview.isHidden = true
                }
            }
            else{
                self.conversationTableview.isHidden = true
            }
        })
        
        updatedMessageRefHandle = messageRef.observe(.childRemoved, with: { (snapshot) in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            if snapshot.value != nil {
                self.conversationTableview.isHidden = false
                let myConversations:NSDictionary = snapshot.value as! NSDictionary
                if myConversations.count > 0 {
                    Utility.dismissProgress()
                    let arr:[String] = myConversations.value(forKey: "member") as! [String]
                    if arr.contains(self.curruntUserID) {
                        self.conversations.remove(myConversations)
                        self.conversationsKey.remove(snapshot.key)
                    }
                    self.conversationTableview.reloadData()
                }
                else{
                    self.conversationTableview.isHidden = true
                }
            }
            else{
                self.conversationTableview.isHidden = true
            }
        })
        
        updatedMessageRefHandle = messageRef.observe(.childChanged, with: { (snapshot) in
            let messageData:NSDictionary = snapshot.value as! NSDictionary
            if snapshot.value != nil {
                self.conversationTableview.isHidden = false
                let myConversations:NSDictionary = snapshot.value as! NSDictionary
                if myConversations.count > 0 {
                    Utility.dismissProgress()
                    let arr:[String] = myConversations.value(forKey: "member") as! [String]
                    if arr.contains(self.curruntUserID) {
                        var count = 0
                        for i in self.conversations {
                            count = count + 1
                            let temp:NSDictionary  = i as! NSDictionary
                            if temp.value(forKey: "id") as! String == myConversations.value(forKey: "id") as! String {
                                self.conversations.replaceObject(at: (count - 1), with: myConversations)
                                self.conversationsKey.replaceObject(at: (count - 1), with: snapshot.key)
                            }
                            else{
                            }
                        }
                        //                        self.conversations.remove(myConversations)
                        //                        self.conversations.add(myConversations)
                        //                        self.conversationsKey.remove(snapshot.key)
                        //                        self.conversationsKey.add(snapshot.key)
                    }
                    self.conversationTableview.reloadData()
                }
                else{
                    self.conversationTableview.isHidden = true
                }
            }
            else{
                self.conversationTableview.isHidden = true
            }
        })
    }
    
    //MARK:- Tableview Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.conversations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationTableViewCell") as! ConversationTableViewCell
        let data:NSDictionary = self.conversations[indexPath.row] as! NSDictionary
        cell.nameLabel.text = data.value(forKey: "conversationName") as? String
        
        let epocTime = TimeInterval(data.value(forKey: "timestamp") as! String)! / 1000
        let unixTimestamp = Date(timeIntervalSince1970: epocTime)
        let secondDate = unixTimestamp
        let myResponse = Date().offsetDate(from: secondDate)
        cell.timeStampLabel.text = myResponse
        
        let remove:UIButton = UIButton()
        remove.backgroundColor = UIColor(red: 35/255, green: 155/255, blue: 216/255, alpha: 1.0)//UIColor.white
        remove.tag = indexPath.row
        remove.setImage(UIImage(named:"delete"), for: .normal)
        remove.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        remove.imageEdgeInsets = UIEdgeInsetsMake(0.0, -5.0, 0.0, 0.0)
        remove.layer.cornerRadius = remove.frame.width / 2;
        remove.clipsToBounds = true
        remove.layer.masksToBounds = true
        remove.addTarget(self, action: #selector(RemoveRecordedFile(sender:)), for: .touchUpInside)
        
        remove.frame = CGRect(x: self.conversationTableview.frame.size.width+10, y:0.0, width: 80, height: cell.mainScrollView.frame.size.height + 1)
        cell.mainScrollView.addSubview(remove)
        
        cell.mainScrollView.contentSize = CGSize(width: remove.frame.size.width + remove.frame.origin.x, height: (cell.mainScrollView.frame.size.height))
        
        cell.mainScrollView.tag = indexPath.row
        cell.mainScrollView.delegate = self
        cell.mainScrollView.setContentOffset(CGPoint.zero, animated: false)
        cell.mainScrollView.delaysContentTouches = false
        cell.mainScrollView.decelerationRate = UIScrollViewDecelerationRateNormal
        cell.mainScrollView.bounces = false
        
        cell.goToChatButtonCLick.tag = indexPath.row
        cell.goToChatButtonCLick.addTarget(self, action: #selector(chatNavigation(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func chatNavigation(sender:UIButton){
        let data:NSDictionary = self.conversations[sender.tag] as! NSDictionary
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController1") as! ChatViewController1
        vc.userID = (data.value(forKey: "userId") as? String)!
        vc.channelID = (data.value(forKey: "id") as? String)!
        vc.conversationName = (data.value(forKey: "conversationName") as? String)!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    // MARK: Delete Conversation
    @objc func RemoveRecordedFile(sender:UIButton){
        if Utility.checkInternet() {
            let data:NSDictionary = self.conversations[sender.tag] as! NSDictionary
            let alert:UIAlertController = UIAlertController(title: "", message: "are you sure want to delete conversation ?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
                Utility.showProgress("")
                let id = data.value(forKey: "id")
                if let exerciseName = id {
                    let ref = Database.database().reference().child("conversation")
                    ref.queryOrdered(byChild: "id").queryEqual(toValue: exerciseName).observe(.childAdded, with: { (snapshot) in
                        Utility.dismissProgress()
                        snapshot.ref.removeValue(completionBlock: { (error, reference) in
                            if error != nil {
                                print("There has been an error:\(error?.localizedDescription ?? "")")
                            }
                            else{
//                                self.conversations.removeObject(at: sender.tag)
                                self.conversationTableview.reloadData()
                            }
                        })
                    })
                }
            }))
            alert.addAction(UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.cancel, handler: { (action) in
            }))
            self.present(alert, animated: true, completion: nil)
        }
        else{
            Utility.showAlert("Internet Connection!", message: "Please check internet connection and retry.", viewController: self)
        }
    }
    
    //MARK:- Button Click Events
    @IBAction func menuButtonClick(_ sender: UIButton) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func addConversationButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "StartConverstionViewController") as! StartConverstionViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func okButtonClick(_ sender: UIButton) {
        self.popupView.isHidden = true
    }
}
