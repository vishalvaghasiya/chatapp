//
//  ForgotPasswordViewController.swift
//  CivilChat
//
//  Created by admin on 30/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController, UtilityDelegate {

    @IBOutlet var emailTextfield: UITextField!
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendComfirmationButtonClick(_ sender: UIButton) {
//        Utility.showProgress("")
//        if self.isValidEmail(testStr: self.emailTextfield.text!){
//            let dict: NSMutableDictionary = NSMutableDictionary()
//            dict.setObject(self.emailTextfield.text!, forKey:DB_EMAIL as NSCopying)
//            
//            let headers = [ "Content-Type": "application/json" ]
//            var request =  URLRequest(url: URL(string: URL_FORGOTPASSWORD)!)
//            request.httpMethod = "POST"
//            request.httpBody = try? JSONSerialization.data(withJSONObject: dict)
//            request.allHTTPHeaderFields = headers
//        
//            utility.UserForgotPassword(request: request, apiType: .User_ForgotPassword)
//            
//        }
//        else{
//        Utility.showAlert("Message", message: "Please enter valid email address.", viewController: self)
//        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_ForgotPassword:
                let alert = UIAlertController(title: "", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("Message", message: error, viewController: self)
        }
    }
    
}
