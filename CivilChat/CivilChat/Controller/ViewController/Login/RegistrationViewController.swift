//
//  RegistrationViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import Alamofire
import Parse
class RegistrationViewController: UIViewController , UtilityDelegate{

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailAddressTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var spouseTextField: UITextField!
    
    
    var emailTrim:String = String()
    var spouseEmail_trim:String = String()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Button Click Events
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registrationButtonClick(_ sender: UIButton) {
        if validation() {
            Utility.createUser(email: self.emailAddressTextField.text!, password: self.passwordTextField.text!, name: self.nameTextField.text!, partneremail: spouseTextField.text!) { (success, error) in
                if success {
                    AppData.sharedInstance.user = PFUser.current() as? User
                    let defaults = UserDefaults.standard
                    defaults.set(AppData.sharedInstance.user?.email, forKey: "UserID")
                    defaults.set(AppData.sharedInstance.user?.partneremail, forKey: "partnerID")
                    defaults.set(AppData.sharedInstance.user?.name, forKey: "UserName")
                    AppData.sharedInstance.partnerID = (AppData.sharedInstance.user?.partneremail)!
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    Utility.showAlert("SIGNUP", message: error.debugDescription, viewController: self)
                }
            }
            
        }
        
        //        if validation() {
        //            Utility.showProgress("")
        //            let dict: NSMutableDictionary = NSMutableDictionary()
        //            dict.setObject(emailAddressTextField.text!, forKey:DB_EMAIL as NSCopying)
        //            dict.setObject(spouseTextField.text!, forKey:DB_SPOUSE_EMAIL as NSCopying)
        //            dict.setObject(passwordTextField.text ?? "", forKey:DB_PASSWORD as NSCopying)
        //            dict.setObject(nameTextField.text!, forKey:DB_FIRSTNAME as NSCopying)
        //
        //            let headers = [ "Content-Type": "application/json" ]
        //            var request =  URLRequest(url: URL(string: URL_REGISTRATION)!)
        //            request.httpMethod = "POST"
        //            request.httpBody = try? JSONSerialization.data(withJSONObject: dict)
        //            request.allHTTPHeaderFields = headers
        //            utility.UserRegisterDetails(request: request, apiType: .User_Registration)
        //        }
    }
    
    //MARK:- Validation Function
    func validation() -> Bool{
        if self.emailAddressTextField.text != "" && self.spouseTextField.text != "" && self.nameTextField.text != "" && self.passwordTextField.text != "" {
            if isValidEmail(testStr: self.emailAddressTextField.text!) && isValidEmail(testStr: self.spouseTextField.text!){
                return true
            }
            else{
                if isValidEmail(testStr: self.emailAddressTextField.text!){
                }
                else{
                    Utility.showAlert("Message", message: "Please enter valid email address.", viewController: self)
                }
                
                if isValidEmail(testStr: self.spouseTextField.text!){
                }
                else{
                    Utility.showAlert("Message", message: "Please enter valid spouse email address.", viewController: self)
                }
                return false
            }
        }
        else if self.emailAddressTextField.text == "" && self.spouseTextField.text == "" && self.nameTextField.text == "" && self.passwordTextField.text == ""{
            Utility.showAlert("Message", message: "all field reuqured.", viewController: self)
            return false
        }
        else if self.emailAddressTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter email address.", viewController: self)
            return false
        }
        else if self.passwordTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter password.", viewController: self)
            return false
        }
        else if self.nameTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter name.", viewController: self)
            return false
        }
        else if self.spouseTextField.text == "" {
            Utility.showAlert("Message", message: "Please enter spouse email address.", viewController: self)
            return false
        }
        return false
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_Registration:
                let alert = UIAlertController(title: "", message: error, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
                    self.navigationController?.pushViewController(vc, animated: true)
                }))
                self.present(alert, animated: true, completion: nil)
                break
            default:
                break
            }
        }
        else{
            Utility.showAlert("Message", message: error, viewController: self)
        }
    }
}
