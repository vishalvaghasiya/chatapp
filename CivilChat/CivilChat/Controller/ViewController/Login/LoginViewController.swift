//
//  LoginViewController.swift
//  CivilChat
//
//  Created by admin on 29/12/17.
//  Copyright © 2017 Onus. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAuth
import FirebaseCore
import Parse
class LoginViewController: UIViewController , UtilityDelegate{

    @IBOutlet var emailTexField: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    
    var usernameTrim:String = String()
    
    var utility: Utility = Utility()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.utility.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK:- Button Click Events
    @IBAction func loginButtonClick(_ sender: UIButton) {
        if self.validation() {
            Utility.showProgress("")
            PFUser.logInWithUsername(inBackground: emailTexField.text!, password: passwordTextfield.text!, block: { (users, error) in
                Utility.dismissProgress()
                if ((users) != nil) {
                    AppData.sharedInstance.user = PFUser.current() as? User
                    let defaults = UserDefaults.standard
                    defaults.set(AppData.sharedInstance.user?.email, forKey: "UserID")
                    defaults.set(AppData.sharedInstance.user?.partneremail, forKey: "partnerID")
                    defaults.set(AppData.sharedInstance.user?.name, forKey: "UserName")
                    AppData.sharedInstance.userID = (AppData.sharedInstance.user?.email)!
                    AppData.sharedInstance.partnerID = (AppData.sharedInstance.user?.partneremail)!
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
                    self.navigationController?.pushViewController(vc, animated: true)
//                    AppData.sharedInstance.user = PFUser.current() as? User
//                    UserDefaults.standard.set(AppData.sharedInstance.user?.mobile!, forKey: "mobile")
//                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "AddViewController") as! AddViewController
//                    let navigationController = UINavigationController(rootViewController: vc)
//                    UIApplication.shared.keyWindow!.rootViewController = navigationController
                } else {
                    Utility.showAlert(self.title, message: error.debugDescription, viewController: self)
                }
            })
//            Auth.auth().signIn(withEmail: emailTexField.text!, password: passwordTextfield.text!) { (user, error) in
//                Utility.dismissProgress()
//                if user != nil {
//                    UserDefaults.standard.set(user?.uid, forKey: "UserID")
//                    AppData.sharedInstance.userID = (user?.uid)!
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
//                    self.navigationController?.pushViewController(vc, animated: true)
//                } else {
//                    Utility.showAlert("Oops", message: error?.localizedDescription, viewController: self)
//                }
//            }
        }
    }
    
    @IBAction func signUpButtonClick(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func forgotPasswordButtonClick(_ sender: UIButton) {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func validation() -> Bool{
        if self.emailTexField.text != "" && self.passwordTextfield.text != ""{
            return true
        }
        else if self.emailTexField.text == "" && self.passwordTextfield.text == "" {
             Utility.showAlert("Message", message: "all field reuqured.", viewController: self)
            return false
        }
        else if self.emailTexField.text == "" {
             Utility.showAlert("Message", message: "Please enter email.", viewController: self)
             return false
        }
        else if self.passwordTextfield.text == "" {
             Utility.showAlert("Message", message: "Please enter password.", viewController: self)
             return false
        }
        return false
    }
    
    // MARK: - Api Call Completion Delegate
    func apiCallCompleted(_ success: Bool, data: NSDictionary?, error: String?, apiType: APIType) {
        Utility.dismissProgress()
        if(success) {
            switch apiType {
            case .User_Login:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "MyConverstionViewController") as! MyConverstionViewController
                self.navigationController?.pushViewController(vc, animated: true)
                break
            default:
                break
            }
        }
        else{
                Utility.showAlert("Message", message: error, viewController: self)
        }
    }
}

