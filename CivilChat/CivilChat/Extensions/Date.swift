//
//  Date.swift
//  CivilChat
//
//  Created by admin on 13/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import Foundation
extension Date {
    
    static func getFormattedDate(string: String) -> String{
        let dateFormatter = DateFormatter();
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz";
        let formateDate = dateFormatter.date(from: string)!;
        dateFormatter.dateFormat = "dd-MM-yyyy";
        return dateFormatter.string(from: formateDate);
    }
    
    static func date(string : String, format : String) -> Date {
        let dateFormatter : DateFormatter = DateFormatter();
        dateFormatter.dateFormat = format;
        return dateFormatter.date(from: string)!;
    }
    
    func add(minutes: Int) -> Date {
        return Calendar(identifier: .gregorian).date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    
    func date(from date: Date) -> Date {
        return Calendar.current.dateComponents([.calendar], from: date, to: self).date!
    }
    
    //MARK: Ony Second Used
    func offset(from date: Date) -> String {
         if minutes(from: date) > 0 { return "\(minutes(from: date))" }
        return "0"
    }
    
    // Conversation Time Function
    func offsetDate(from date: Date) -> String {
        if minutes(from: date) >= 0 && minutes(from: date) < 60 {
            return "\(minutes(from: date)) min"
        }
        else if hours(from: date) > 0 && hours(from: date) < 24 {
            let timeStamp = date.timeIntervalSince1970 * 1000
            let epocTime = TimeInterval(timeStamp) / 1000
            let formatter = DateFormatter()
            let unixTimestamp = NSDate(timeIntervalSince1970: epocTime)
            formatter.dateFormat = "hh:mm a"
            let strtime = formatter.string(from: unixTimestamp as Date)
            return "\(strtime)"
        }
        else if days(from: date) == 1 {
            return "yesterday"//"\(days(from: date))"
        }
        else{
            let myString = (String(describing: date))
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz";
            let formateDate = dateFormatter.date(from: myString)!;
            dateFormatter.dateFormat = "dd-MM-yyyy";
            
            return dateFormatter.string(from: formateDate)
        }
        return "0"
    }
    
    
}
