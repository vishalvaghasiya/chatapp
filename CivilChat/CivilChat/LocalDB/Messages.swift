//
//  Messages.swift
//  CivilChat
//
//  Created by admin on 04/01/18.
//  Copyright © 2018 Onus. All rights reserved.
//

import Foundation
import UIKit
class Messages {
    
    var messageid:String!
    var ismediamessage:String!
    var read:String!
    var senderId:String!
    var senderName:String!
    var text:String!
    var timestamp:String!
    var sendervisible:String!
    var receivervisible:String!
    var channelid:String!
    
}
